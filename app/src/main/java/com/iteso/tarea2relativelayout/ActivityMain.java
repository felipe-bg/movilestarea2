package com.iteso.tarea2relativelayout;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

public class ActivityMain extends AppCompatActivity {

    ImageButton likeButton;
    Button addToCart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        likeButton = findViewById(R.id.activity_main_like_button);
        addToCart = findViewById(R.id.activity_relative_add_to_cart);

        likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast likeToast = Toast.makeText(ActivityMain.this,
                        getResources().getString(R.string.like_message), Toast.LENGTH_SHORT);
                likeToast.setGravity(Gravity.CENTER,0,0);
                likeToast.show();
            }
        });

        addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToCart.setText(R.string.added_to_cart);

                Snackbar cartSnackbar = Snackbar.make(
                        findViewById(R.id.activity_relative_add_to_cart),
                        getResources().getString(R.string.added_to_cart),
                        Snackbar.LENGTH_LONG);
                cartSnackbar.setAction(R.string.undo, new AddToCartUndo());
                cartSnackbar.show();
            }
        });
    }
}
